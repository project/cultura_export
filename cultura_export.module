<?php

/**
 * @file
 * Cultura export
 */

/**
 * Implements hook_menu().
 */
function cultura_export_menu() {
  if ($token = variable_get('cultura_export', FALSE)) {
    $items["cultura/export/$token"] = array(
      'page callback' => 'cultura_export_xml',
      'access callback' => TRUE,
    );
    return $items;
  }
}

/**
 * Implements hook_views_api().
 */
function cultura_export_views_api($module, $api) {
  if ($module == 'views' && $api == 'views_default') {
    return array('version' => 3);
  }
}

/**
 * Implements hook_block_info().
 */
function cultura_export_block_info() {
  $blocks = array();

  $blocks['export'] = array(
    'info' => t('Export'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function cultura_export_block_view($delta) {
  switch ($delta) {
    case 'export':
      return cultura_export_block();
  }
}

/**
 * Page callback for XML export.
 */
function cultura_export_xml() {
  $nodes = views_embed_view('culturaexchange_export', 'culturaexchange_xml');
  $nodes = substr($nodes, strlen('<?xml version="1.0" encoding="UTF-8" ?>'));
  $comments = views_embed_view('cultura_comment_export', 'culturaexchange_comment_xml');
  $comments = substr($comments, strlen('<?xml version="1.0" encoding="UTF-8" ?>'));
  print '<?xml version="1.0" encoding="UTF-8" ?>' . "\n";
  print '<site_export>' . "\n";
  print '<site_url>' . $GLOBALS['base_url'] . '</site_url>' . "\n";
  print '<host_school>' . variable_get('cultura_host_school') . '</host_school>' . "\n";
  print '<guest_school>' . variable_get('cultura_guest_school') . '</guest_school>' . "\n";
  print '<cultura_year>' . variable_get('cultura_year') . '</cultura_year>' . "\n";
  print '<cultura_semester>' . variable_get('cultura_semester') . '</cultura_semester>' . "\n";
  print $nodes;
  print $comments;
  print '</site_export>';
}

/**
 * Returns registration token block for instructors.
 */
function cultura_export_block() {
  $block['subject'] = t('Export exchange');

  // If the export URL is not yet published, show an 'Export' button.
  if (variable_get('cultura_export', FALSE) == FALSE) {
    $block['content'] = drupal_get_form('cultura_export_form');
  }
  else {
    $token = variable_get('cultura_export');
    $link = url("cultura/export/$token", array('absolute' => TRUE));
    $html = '<dl>';
    $html .= "<dt>" . t('Private URL to provide to Cultura archive') . "</dt>";
    $html .= "<dd>$link</dd>";
    $html .= '</dl>';
    $html .= '<p class="description">Provide this link to an institution which will host
      your archives, such as <a href="http://cultura.mit.edu/">the Cultura project at
      MIT</a>.</p>';
    $block['content']['#markup'] = $html;
  }

  return $block;
}

/**
 * Form constructor for the registration tokens form.
 *
 * @see cultura_registration_tokens_form_validate()
 * @see cultura_registration_tokens_form_submit()
 *
 * @ingroup forms
 */
function cultura_export_form() {
  $form = array();

  $form['#description'] = t('Generate private URL to provide to Cultura archive');
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate export URL'),
  );

  return $form;
}

/**
 * Form submission handler for cultura_registration_tokens_form().
 *
 * @see cultura_registration_tokens_form()
 */
function cultura_export_form_submit($form, &$form_state) {
  $token = user_password(16);
  variable_set('cultura_export', $token);
  menu_rebuild();
  drupal_set_message(t('Your export link has been generated.'));
}

/**
 * Implements hook_form_FORM_ID_alter() for system_site_information_settings.
 */
function cultura_export_form_system_site_information_settings_alter(&$form, &$form_state) {
  $form['site_information']['cultura_year'] = array(
    '#type' => 'textfield',
    '#title' => 'Cultura year',
    '#default_value' => variable_get('cultura_year', date('Y')),
    '#required' => TRUE,
    '#description' => t('For example, %year.', array('%year' => date('Y'))),
    '#weight' => -15,
  );
  $semester = (date('z') < 183) ? 'Spring' : 'Fall';
  $form['site_information']['cultura_semester'] = array(
    '#type' => 'textfield',
    '#title' => 'Cultura semester',
    '#default_value' => variable_get('cultura_semester', $semester),
    '#required' => TRUE,
    '#description' => t('For example, %semester.', array('%semester' => $semester)),
    '#weight' => -14,
  );
  $form['site_information']['cultura_host_school'] = array(
    '#type' => 'textfield',
    '#title' => 'Cultura host school',
    '#default_value' => variable_get('cultura_host_school', ''),
    '#description' => t('Your school or university.'),
    '#required' => TRUE,
    '#weight' => -13,
  );
  $form['site_information']['cultura_guest_school'] = array(
    '#type' => 'textfield',
    '#title' => 'Cultura guest school',
    '#default_value' => variable_get('cultura_guest_school', ''),
    '#description' => t('The school or university with which you are partnering.'),
    '#required' => TRUE,
    '#weight' => -12,
  );
}